(ns clojure-noise.core
  (:gen-class))

;; https://guide.clojure.style/
;; https://en.wikipedia.org/wiki/Perlin_noise

(def ^:const tau "Tau = 2*Pi, a full circle in radians." 6.283185)

(def ^:const epsilon "Epsilon = Very Small, a value that can be used when calculations need a little nudge." 0.000001)

(defn random-unit-vector
  "Calculates a random 2D unit vector in [x, y] form."
  []
  (let [phi (rand tau)]
    [(Math/cos phi) (Math/sin phi)]))

(defn gradient-grid
  "Calculates a 2D grid of the specified dimensions, where each point stores a random 2D unit vector in [x, y] form.
  The grid is represented as a list of rows, each of which is a list of points.
  Use the `grid-height`, `grid-width`, and `grid-elem` functions to simply access grid data."
  [width height]
  (for [_ (range height)]
    (for [_ (range width)]
      (random-unit-vector))))

(defn grid-height "Returns the height of `grid`."
  [grid]
  (count grid))

(defn grid-width "Returns the width of `grid`."
  [grid]
  (if (empty? grid) 0 (count (nth grid 0))))

(defn grid-elem "Returns the grid element located at [`x`, `y`]."
  [grid [x y]]
  (let [row (nth grid y (last grid))]
    (nth row x (last row))))

(defn frac "Returns only the fractional part of a supplied value `x`."
  [x]
  (- x (Math/floor x)))

(defn to-grid "Scales the given `x` and `y` values from a space defined by `width` and `height` onto the given grid."
  [x y width height grid]
  (let [x-percent (/ x width) y-percent (/ y height)]
    [(* x-percent (grid-width grid))
     (* y-percent (grid-height grid))]))

(defn dot-product "Computes the dot product of the given 2D vectors."
  [[a b] [c d]]
  (+ (* a c) (* b d)))

(defn distance-vector "Computes the difference of the given 2D vectors."
  [[a b] [c d]]
  [(float (- c a))
   (float (- d b))])

(defn lerp "Linearly interpolates between values `a` and `b` by `t`."
  [a b t]
  (+ a (* t (- b a))))

(defn lerp-rgba "Linearly interpolates between byte-encoded colours `a` and `b` by `t`."
  [a b t]
  (let
   [ab (bit-and a 0xFF)
    ag (bit-and (bit-shift-right a 8) 0xFF)
    ar (bit-and (bit-shift-right a 16) 0xFF)
    aa (bit-and (bit-shift-right a 24) 0xFF)
    bb (bit-and b 0xFF)
    bg (bit-and (bit-shift-right b 8) 0xFF)
    br (bit-and (bit-shift-right b 16) 0xFF)
    ba (bit-and (bit-shift-right b 24) 0xFF)]
    (bit-or
     (int (lerp ab bb t))
     (bit-shift-left (int (lerp ag bg t)) 8)
     (bit-shift-left (int (lerp ar br t)) 16)
     (bit-shift-left (int (lerp aa ba t)) 24))))

(defn dist-gradient-dot
  "Computes the dot product of the distance vector between `xy` and `uv`,and the gradient vector in the grid at `uv`."
  [grid xy uv]
  (dot-product (distance-vector xy uv) (grid-elem grid uv)))

(defn perlin
  "Computes perlin noise in the range [0, 1],
  for the point [`x`, `y`] in an image of the given dimensions,
  on the given `grid`."
  [x y width height grid]
  (let
   [in-grid (to-grid x y width height grid)
    dx (frac (in-grid 0))
    dy (frac (in-grid 1))
    x0y0 [(int (Math/floor (in-grid 0))) (int (Math/floor (in-grid 1)))]
    x0y1 [(int (Math/floor (in-grid 0))) (int (Math/ceil (+ epsilon (in-grid 1))))]
    x1y0 [(int (Math/ceil (+ epsilon (in-grid 0)))) (int (Math/floor (in-grid 1)))]
    x1y1 [(int (Math/ceil (+ epsilon (in-grid 0)))) (int (Math/ceil (+ epsilon (in-grid 1))))]
    x0y0-dist-gradient-dot (dist-gradient-dot grid in-grid x0y0)
    x0y1-dist-gradient-dot (dist-gradient-dot grid in-grid x0y1)
    x1y0-dist-gradient-dot (dist-gradient-dot grid in-grid x1y0)
    x1y1-dist-gradient-dot (dist-gradient-dot grid in-grid x1y1)
    x0-grad (lerp x0y0-dist-gradient-dot x0y1-dist-gradient-dot dy)
    x1-grad (lerp x1y0-dist-gradient-dot x1y1-dist-gradient-dot dy)
    grad (lerp x0-grad x1-grad dx)]
    (/ (+ 1 grad) 2)))

(defn fractal
  "Computes a full image of fractal noise of depth `d` in the range [0, 1],
  for an image of the given dimensions.
  `initial-scale` determines the scale of the noise at depth 0."
  [width height depth initial-scale]
  (let
   [grids (for
           [d (range depth)]
            (gradient-grid 
             (* (Math/pow 2 d) (/ width initial-scale))
             (* (Math/pow 2 d) (/ height initial-scale))))]
    (for
     [y (range height) x (range width)]
      (/ (apply + (map (fn [grid] (perlin x y width height grid)) grids)) depth))))

(defn colour-perlin
  "Converts noise in the range [0, 1] to colours linearly interpolated between `a` and `b`."
  [a b noise]
  (map (fn [e] (lerp-rgba a b e)) noise))

(defn -main
  "Prompts for user input and generates a noise image using the user's parameters."
  [& _]
  (println "+-----------------------------------------+")
  (println "| SUPER FRACTAL NOISE GENERATOR ADVANCE!! |")
  (println "+-----------------------------------------+")
  (let
   [width (do (print "Enter image width: ") (flush) (java.lang.Long/parseLong (read-line) 10))
    height (do (print "Enter image height: ") (flush) (java.lang.Long/parseLong (read-line) 10))
    depth (do (print "Enter fractal depth: ") (flush) (java.lang.Long/parseLong (read-line) 10))
    scale (do (print "Enter initial noise scale: ") (flush) (java.lang.Long/parseLong (read-line) 10))
    color-a (do (print "Enter 8-digit ARGB hexadecimal value for the first colour: 0x") (flush) (java.lang.Long/parseLong (read-line) 16))
    color-b (do (print "Enter 8-digit ARGB hexadecimal value for the second colour: 0x") (flush) (java.lang.Long/parseLong (read-line) 16))
    output-file-name (do (print "Enter file name to save generated image in: ") (flush) (read-line))
    noise (do (println "Generating...") (fractal width height depth scale))
    buffer (new java.awt.image.BufferedImage width height java.awt.image.BufferedImage/TYPE_INT_ARGB)]
    (.setRGB buffer 0, 0, width, height, (int-array (colour-perlin color-a color-b noise)), 0, width)
    (javax.imageio.ImageIO/write buffer "png" (new java.io.File output-file-name)))
  (println "Complete!"))
