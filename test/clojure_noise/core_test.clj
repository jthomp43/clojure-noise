(ns clojure-noise.core-test
  (:require [clojure.test :refer :all]
            [clojure-noise.core :refer :all]))

(defn is-unit-vector
  [vec]
  (> 0.0001 (Math/abs (- 1 (Math/sqrt (+ (Math/pow (vec 0) 2) (Math/pow (vec 1) 2))))))
  )

(deftest random-unit-vector-test
  (testing "Testing random unit vectors really are unit vectors."
    (is (every? is-unit-vector (map (fn [_] (random-unit-vector)) (range 100) )))
    ))
